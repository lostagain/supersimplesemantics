package com.darkflame.client;

import java.awt.List;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import com.darkflame.client.semantic.QueryEngine.DoSomethingWithNodesRunnable;
import com.darkflame.client.semantic.SSSUtilities;
import com.darkflame.client.semantic.SSSIndex;
import com.darkflame.client.semantic.SSSNode;
import com.darkflame.client.semantic.SSSNodesWithCommonProperty;
import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;
/**
 * This class will help manage the exporting of loading semantic data back to SSS formated files
 * @author Tom
 *
 */
public class SSSExporter {
	
	static Logger Log = Logger.getLogger("sss.SSSExporter");
	//gather all prefixs to put at the start of the index
	
	
	//get location for new index
	//if one exists already, provide option to back up _bak or such
	//open a new file there for writing
		
	//loop over all known common property sets
	//all sets with a single triplet become a newline on the index file. (amend directly)
	//all sets with many triplets become a subfile + reference to that file on the index file (amend directly on index, but dont write
	//the associated file till after the index file is finished and closed)
	
	
	
	//TODO: loading data back in will need more controll
	//I prepose options for
	// Clear all - clears all existing data first
	// Merge     - just merges new data in, removing nothing (so some nodes may get contradicting property values)
	// Update    - any common property set with the same source as its domain gets updated to the new value
	
	//TODO: Also need a "clear domain" option, which clears everything from a specific source
	
	//Overall goal for now; make it workable for a save game system should test this nicely.
	
	
	
	//Notes below;
	
	//Player  score:12   (source; demo.ntlist
	//Player  score:56   (source; demo.ntlist
	//(update should make the score be overwritten as source is the same)
	
	
	
	/**
	 * setable for different platforms (sign @ necessity of this)
	 */
	private static String NEWLINECHARACTER = System.getProperty("line.separator");

	/**
	 * Orders sets first by ones with just one subject node, then ones with more then one subject node.
	 * the sets with just one node are ordered alphabetically by subject node Uri				
	 * the sets with more then one subject are ordered alphabetically by predicate node uri+value node uri
	 */
	static Comparator<SSSNodesWithCommonProperty> CommonPropertySetSorter = new Comparator<SSSNodesWithCommonProperty>() {

		@Override
		public int compare(SSSNodesWithCommonProperty o1, SSSNodesWithCommonProperty o2) {
			//we order the sets

			Log.info("ordering: "+o1.toString()+" against "+o2.toString());
			
			//first put all empty sets at the start (should not be any!)
			if (o1.size()==0 && o2.size()>0){
				return -1;
			}
			if (o2.size()==0 && o1.size()>0){
				return 1;
			}
			
			//then sets with just one subject node
			//then sets with more then one subject node
			if (o1.size()==1 && o2.size()>1){
				return -1;
			}
			if (o2.size()==1 && o1.size()>1){
				return 1;
			}
							
			//the sets with just one node are ordered alphabetically by subject node Uri				
			if (o2.size()==1 && o1.size()==1){
				SSSNode o1_firstSubject = o1.get(0);
				SSSNode o2_firstSubject = o2.get(0);
				return o1_firstSubject.getPURI().compareToIgnoreCase(o2_firstSubject.getPURI());
			}
							
			//the sets with more then one subject are ordered alphabetically by predicate node uri+value node uri
			if (o2.size()>1 && o1.size()>1){
				
				SSSNode o1_Pred = o1.getCommonPrec();
				SSSNode o2_Pred = o2.getCommonPrec();
				SSSNode o1_Val = o1.getCommonValue();
				SSSNode o2_Val = o2.getCommonValue();
				
				String o1_idString = o1_Pred.getPURI()+"_"+o1_Val.getPURI();
				String o2_idString = o2_Pred.getPURI()+"_"+o2_Val.getPURI();
				
				return o1_idString.compareToIgnoreCase(o2_idString);
			}
			
			return 0;
		}
	};

	//options?
	static boolean includeUnloadedFiles = true;
	
	static boolean useShortForm = true;
	
	/**
	 * will wrap URIs in greater then and less then signs - but only if not in their shortened prefixed form
	 */
	static boolean useAngledBrackets = true;
	
	// Next goal; Read in a N3 file with angled brackets, ensure data is correctly understood+

	public static boolean saveAllKnowenCommonPropertySetsToFile(String targetNTListLocation, String assumeLocalURI){
		
		//assumeLocalURI should end with #
		if (!assumeLocalURI.endsWith("#")){
			Log.info("assumeLocalURI should end with #,so I am adding one.");
			assumeLocalURI = assumeLocalURI +"#";
		}
		
		
		//get all sets
		Collection<SSSNodesWithCommonProperty> allSets = SSSNodesWithCommonProperty.globalNodesWithPropertyListByPredicate.values();
				
		//file any with local GFE stuff or default values get removed
		//(a bit rough, should be redone neater)
		Iterator<SSSNodesWithCommonProperty> it = allSets.iterator();
		while (it.hasNext()) {
			SSSNodesWithCommonProperty set = (SSSNodesWithCommonProperty) it
					.next();

			Log.info("set: "+set.getDiscription());
			
			String nameSpace = set.getCommonValue().getNameSpace();
			

			Log.info("nameSpace: "+nameSpace);

			if (nameSpace.equalsIgnoreCase("GFE") || nameSpace.equalsIgnoreCase("examples/DefaultOntology.n3")){
				
				it.remove();
			}
		}
		
		//this can be combined with the above - let us build the list as we remove bits we dont want
		ArrayList<SSSNodesWithCommonProperty> listOfCollection = new ArrayList<SSSNodesWithCommonProperty>(allSets);		
		
		
		
		saveCommonPropertySetsToFile(listOfCollection,targetNTListLocation,assumeLocalURI);
		
		return false; //if sucessfull true
	}

	public static boolean saveOneNodesPropertysToFile(SSSNode node, String targetNTListLocation){
		//get all CPS with this node in
		//saveCommonPropertySetsToFile()
		return false; //if sucessfull
	}
	
	
	static class PropertyListFileCreation {
		String fileLocation;
		SSSNodesWithCommonProperty set;
	
		public PropertyListFileCreation(String fileLocation,
				SSSNodesWithCommonProperty set) {
			
			this.fileLocation = fileLocation;
			this.set = set;
			
		}
	}
	
	static ArrayList<PropertyListFileCreation> pendingFilesToCreate = new ArrayList<PropertyListFileCreation>();


	private static SSSNode lastSubject;
	
	/**
	 * note; currently internally creates the file and writes in one go.
	 * For large databases this is bad and really should work by opening and amending a file one line at a time
	 * 
	 * @param sets
	 * @param targetNTListLocation
	 * @return
	 */
	public static boolean saveCommonPropertySetsToFile(final ArrayList<SSSNodesWithCommonProperty> sets, final String targetNTListLocation, final String assumeLocalURI){
		pendingFilesToCreate.clear();
		lastSubject = null;
		
		
		//first we prefetch all sets if requested
		//this is needed as the ordering depends on if the set has one element or more then one element. We dont know
		//this till we load it!		
		final ArrayList<SSSNodesWithCommonProperty> stillToLoad = new ArrayList<SSSNodesWithCommonProperty>(sets);
		Log.info("ensureing all sets are preloaded");
		for (final SSSNodesWithCommonProperty currentSet : sets) {
									
			currentSet.getAllDirectNodesInSet(new DoSomethingWithNodesRunnable() {				
				@Override
				public void run(ArrayList<SSSNode> newnodes, boolean invert) {
					
					stillToLoad.remove(currentSet);
					
					Log.info("loaded: "+currentSet.getDiscription());
					
					if (stillToLoad.size()==0){
						
						Log.info("finnished preloading, ready to generate ntlist file");
						
						generateNTIndexFile(sets,targetNTListLocation,assumeLocalURI);
												
						generateCommonPropertySetFiles(targetNTListLocation,assumeLocalURI);
						
						
					}
				}

			},true);
			
		}
		
		
		
		return false;
	}
	
	protected static void generateCommonPropertySetFiles(final String targetNTListLocation, String assumeLocalURI) {

		String absPath = SuperSimpleSemantics.fileManager.getAbsolutePath(targetNTListLocation);		
		Path path = Paths.get(absPath);
		
		final Path parentFolder = path.getParent();
		
		Log.info("path: "+path);
		Log.info("parentPath: "+parentFolder);
		
		Log.info("createSetFiles: "+pendingFilesToCreate.size());
		
		for (PropertyListFileCreation cpl : pendingFilesToCreate) {
			
			final String targetLocation = cpl.fileLocation;
						
			//option to skip? (must not write them in the index)
			//if not skip load then save all(default)
			Log.info("createSetFile for: "+cpl.set.getCommonPrec()+":"+cpl.set.getCommonValue()+"  "+cpl.set.size()+" loaded:"+cpl.set.isLoaded);
			
			cpl.set.getAllNodesAndSubclassesInSet(new DoSomethingWithNodesRunnable() {				
				@Override
				public void run(ArrayList<SSSNode> newnodes, boolean invert) {
					String fileContents = "";
					
					for (SSSNode cps : newnodes) {
						String line = getNodesURIasString(cps);
												
						fileContents= fileContents +line+ NEWLINECHARACTER;						
					}

					Log.info("targetLocation: "+targetLocation);
					Log.info("parentFolder: "+parentFolder.toString());
					String location = parentFolder.resolve(targetLocation).toString();
					
					SuperSimpleSemantics.fileManager.saveTextToFile(location,fileContents, null, null);
				}
			});
						
		}
		
		
		if (assumeLocalURI!=null){
			SSSUtilities.removePrefix(assumeLocalURI);
		}
		
		
	}

	/**
	 * returns a nodes URI as a string, taking into account if we are asked to use the short form or not
	 * or wrap it in angled brackets or not
	 * 
	 * @param cps
	 * @return
	 */
	protected static String getNodesURIasString(SSSNode node) {
		
		String nodesString = "";
		
		if (useShortForm){
			
			nodesString = node.getShortPURI(); //NOTE: we dont use the inbuilt function to get the short form, as if we do that we cant tell if there was a short form found or not
		
			//if the short form is the same as the original, then clearly a prefix wasn't found
			//in this case, if requested, we add the angled brackets
			if (nodesString==node.getPURI() && useAngledBrackets) {
				
				nodesString = "<"+nodesString+">";
				
			}
			
			
		} else {
		
			nodesString = node.getPURI();
			
			if (useAngledBrackets){
				nodesString = "<"+nodesString+">";
			}
		
		}
		
		
		
		return nodesString;
	}

	private static void generateNTIndexFile(ArrayList<SSSNodesWithCommonProperty> sets, String targetNTListLocation, String assumeLocalURI ) {
		
		//String absPath = SuperSimpleSemantics.fileManager.getAbsolutePath(targetNTListLocation);		
		//Path path = Paths.get(absPath);
		
		//first we prepare the temp local prefixs
		//this should be removed after, as its only used in processing the file
		if (assumeLocalURI!=null){
			SSSUtilities.addPrefix(assumeLocalURI.trim(), "");
		}
		
		
		
		String fileContents = "";
			
		fileContents = getAllPrefixs();
		
		//insert two newlines after for neatness
		fileContents= fileContents +NEWLINECHARACTER;
		fileContents= fileContents +NEWLINECHARACTER;
		
		Log.info("ordering sets for ntlist file");

		//we first order the sets
		//first sets with just one subject node
		//then sets with more then one subject node
		//the sets with just one node are ordered alphabetically by subject node Uri
		//the sets with more then one subject are ordered alphabetically by predicate node uri+value node uri
		Collections.sort(sets, CommonPropertySetSorter);
		
		boolean FirstLine=true; //a flag as the first line shouldn't add a dot before it
		
		//now loop over the ordered set generating lines for the ntlist file		
		for (SSSNodesWithCommonProperty currentSet : sets) {
					
			int size = currentSet.size();
			if (size==0){
				Log.warning("empty set requested to be added to ntindex file. Empty sets really shouldn't exist, let alone be added");
				Log.warning("set was "+currentSet.getDiscription());
				Log.warning("continueing from next set.");
				continue;				
			}
			
			
			
			String line = "";
		
			if (size==1){
				
				SSSNode firstSubject  = currentSet.get(0);		
				SSSNode commonPred    = currentSet.getCommonPrec();
				SSSNode commonValue   = currentSet.getCommonValue();
				
				//first we finish of the last line
				//we do this here because if this current line as the same subject as the last, then the last line should finish with a ;
				//else it finishes with a .
				if (firstSubject==lastSubject) {
					fileContents= fileContents + ";";
				} else {
					
					if (FirstLine){						
						FirstLine=false;
					} else {
						fileContents= fileContents + ".";
					}
					
					fileContents= fileContents + NEWLINECHARACTER; //extra newline after any subject change
				}
				
				line = getNTIndexLineFromTriplet(firstSubject,commonPred,commonValue );


				lastSubject = firstSubject;
			} 
			
			if (size>1){
				if (FirstLine){						
					FirstLine=false;
				}
				
				fileContents= fileContents + ".";
				
				line = getNTIndexLineFromSet(currentSet);
				
				fileContents= fileContents + NEWLINECHARACTER; //extra newline added before set file referances
			} 
			
			

			fileContents= fileContents + NEWLINECHARACTER;
			
			fileContents= fileContents +line;
			
					
				//	String commonPredString  = commonPred.getShortPURI();
				//	String commonValueString = commonValue.getShortPURI();
				
				
					//Next step refractor this more neatly
					//Collapse similiar lines next to eachother
					/*
					String  subject = "";	
					
					if (size==1){
						
						
						
						//}
						
					} else {
						//more complex, as we need to make subfiles
						
						//create suitable filename for set
						String fileName = "";
						if (commonPred==SSSNode.SubClassOf){
							 fileName = "is"+commonValue.getPLabel()+".txt";
						} else {				
							 fileName = "is"+commonPred.getPLabel()+commonValue.getPLabel()+".txt"; //filename of nt list file
						}
						
						//add set to list files pending creation
						// 
						//createListFileForSet(filename,currentSet);
						pendingFilesToCreate.add(new PropertyListFileCreation(fileName,currentSet));
						
						//use the filename as the subject line
						
						 subject = fileName;
						 
						 //extra newline before files
							fileContents= fileContents +NEWLINECHARACTER;
						
					}
					
					String line = subject+" "+commonPredString+" "+commonValueString+".";
					fileContents= fileContents +line+NEWLINECHARACTER;
					
					*/
				}
				
		fileContents= fileContents + "."; //Final dot
		
		SuperSimpleSemantics.fileManager.saveTextToFile(targetNTListLocation, fileContents, null, null);
		

		
		
		
	}

	
	/**
	 * Note; also adds to pending list of files to generate
	 * @param currentSet
	 * @return
	 */
	private static String getNTIndexLineFromSet(SSSNodesWithCommonProperty currentSet) {
		
		//more complex, as we need to make subfiles
		
		//create suitable filename for set
		SSSNode commonValue = currentSet.getCommonValue();
		SSSNode commonPred  = currentSet.getCommonPrec();
		
		String commonValueLabelString = commonValue.getPLabel();
		String commonPredLabelString  = commonPred.getPLabel();
		
		String fileName = "";
		if (commonPred==SSSNode.SubClassOf){
			 fileName = "is"+commonValueLabelString+".txt";
		} else {				
			 fileName = commonPredLabelString+"_"+commonValueLabelString+".txt"; //filename of nt list file. Note currently this might have unsafe characters for files...hmm...
		}
		
		fileName = makeFileNameSafe(fileName);
		
		//add set to list files pending creation
		// 
		//createListFileForSet(filename,currentSet);
		pendingFilesToCreate.add(new PropertyListFileCreation(fileName,currentSet));
		
		String predicate = getNodesURIasString(commonPred);
		String value = getNodesURIasString(commonValue);
		
		
		//use the filename as the subject line
		String line = fileName+" "+predicate+" "+value; //note no dot, gets added at start of next loop
		
		
		
		return line;
	}
	
	

	/**
	 * To assist in making filenames, this will remove or change any unsafe characters (WIP)
	 * TODO: Ensure filename isn't already used, if it is add a number at the end
	 * 
	 * @param pLabel
	 * @return
	 */
	private static String makeFileNameSafe(String labelString) {
		
		// = is dealt with seperately to help readability
		labelString = CharMatcher.is('=').replaceFrom(labelString, "_equals_");
		
		labelString = CharMatcher.anyOf("\\/:*?\"<>|").replaceFrom(labelString, "");
		
		
		
		return labelString;
	}

	/**
	 * Note; This doesn't include the "." or ";" at the end, as that will be determined by if the next property set has the same subject or not
	 * @param subjectNode
	 * @param commonPred
	 * @param commonValue
	 * @return
	 */
	private static String getNTIndexLineFromTriplet(SSSNode subjectNode, SSSNode commonPred, SSSNode commonValue) {

		
		String subject   = getNodesURIasString(subjectNode);
		String predicate = getNodesURIasString(commonPred);
		String value     = getNodesURIasString(commonValue);
						
		subject          = quoteIfNeeded(subject);
		predicate        = quoteIfNeeded(predicate);
		value            = quoteIfNeeded(value);
				 
		String line ="";
		
		
		if (lastSubject==subjectNode){
			
			 //pad the start of the string to the length of the last specified subject
			 //(helps readability)
			String padding = "";
			if (useShortForm){
				 padding = Strings.padStart("", lastSubject.getShortPURI().length()+3, ' '); //this line just makes a string of spaces the correct length
			} else {
				 padding = Strings.padStart("", lastSubject.getPURI().length()+3, ' '); //this line just makes a string of spaces the correct length
				
			}
			
			 line = padding+predicate+" "+value;
			 
		} else {
			 line = subject+" "+predicate+" "+value;
		}
		
		
		return line;
	}

	/**
	 * umm....not sure when quotes are needed.
	 * It should be fairly harmless to quote even when not needed, unless possibly its full URI 
	 * @param name
	 * @return
	 */
	private static String quoteIfNeeded(String name) {
		
		//quote if not a URI
		//if (!name.contains("#")){ //not a good detection, what if # is part of a quoted string?
		//	name = "\""+name+"\""; 
		//
		
		//Quote only after the first colon or : ? 
		//is data from pURI really unambigious? should it be kept quoted maybe?
		
		return name;
	}

	private static void createSetFiles(final Path parentFolder,final ArrayList<PropertyListFileCreation> pendingFilesToCreate) {
		
		
	}

	/**
	 * NOTE: THIS WILL OVERWRITE WHATS AT THAT LOCATION
	 * @param targetNTListLocation
	 * @return
	 */
	public static boolean saveTestDataFile(String targetNTListLocation){
		
		String testData = getAllPrefixs(); //just using the prefix list for now in future we will have sample data and use the "saveOneNodesPropertysToFile" function
		
		testData = testData + NEWLINECHARACTER;
		testData = testData + "Number of Property Sets = " +SSSNodesWithCommonProperty.globalNodesWithPropertyListByPredicate.size();
		testData = testData + NEWLINECHARACTER;
		testData = testData + "Number of Nodes Sets = " +SSSNode.getAllKnownNodes().size();
		testData = testData + NEWLINECHARACTER;
		
		final String baseURI = "examples/testData.n3#";
		
		//test nodes
		SSSNode player		= SSSNode.createSSSNode("Player","Player", baseURI);		
		SSSNode playerScore	= SSSNode.createSSSNode("Player Score","PlayerScore", baseURI);
		SSSNode Inventory	= SSSNode.createSSSNode("Inventory","Inventory", baseURI);
		SSSNode Capacity	= SSSNode.createSSSNode("Capacity", "Capacity", baseURI);
		SSSNode Integer		= SSSNode.createSSSNode("Integer", "Integer", baseURI);

		SSSNode ten          = SSSNode.createSSSNode("Ten","10", baseURI,new SSSNode[]{Integer} );

		//(random objects for the inventory to contain)
		SSSNode apple      = SSSNode.createSSSNode("apple","apple", baseURI);
		SSSNode lockpick   = SSSNode.createSSSNode("lock pick","lockpick", baseURI);
		SSSNode lamp       = SSSNode.createSSSNode("lamp","lamp", baseURI);
		SSSNode sword      = SSSNode.createSSSNode("sword","sword", baseURI);
		SSSNode writting   = SSSNode.createSSSNode("\"a lot of mysterious writting\"","\"a lot of mysterious writting\"", baseURI);
		
		//blahblah#lot of mysterious writing
		//blahblah#www.blah.com:wtfisThis
		//blahblah#"www.blah.com:wtfisThis" < -- internal storage should change to uris with quotes as its ess ambiguous and neater
		//NOTE: we should also optionally have auto-quoting URIs when they contain spaces?
		
		//assign what the player has
		SSSNode ownedBy                       = SSSNode.createSSSNode("ownedBy","ownedBy", baseURI);
		SSSNodesWithCommonProperty playersHas = SSSNodesWithCommonProperty.createSSSNodesWithCommonProperty(ownedBy, player, new SSSNode[]{Inventory});

		
		//give the inventory its propertys (including what it contains)
		SSSNodesWithCommonProperty inventoryHas = SSSNodesWithCommonProperty.createSSSNodesWithCommonProperty(ownedBy, Inventory, new SSSNode[]{apple,lockpick,lamp,sword,writting});
		//add it to the list of things with capacity ten (not this is done the other way around - we arnt giving it the property of capacity 10, we are adding it to the list of things that are capacity 10)
		//testing should include semantics defined both ways
		SSSNodesWithCommonProperty CapcityTen = SSSNodesWithCommonProperty.createSSSNodesWithCommonProperty(Capacity, ten, new SSSNode[]{Inventory});
		
		saveAllKnowenCommonPropertySetsToFile(targetNTListLocation,baseURI);
		
				
		
		//SuperSimpleSemantics.fileManager.saveTextToFile(targetNTListLocation, testData, null, null);
		
		return false;
	}
	
	
	public static String getAllPrefixs(){
		
		String prefixs = "";
		HashMap<String, String> prefixArray = SSSUtilities.getPrefixs();
		for (String uri : prefixArray.keySet()) {
			
			String prefix = prefixArray.get(uri);
			String newline = "@prefix "+prefix+" <"+uri+">";
			prefixs = prefixs + newline+NEWLINECHARACTER ;
		}
		
		return prefixs;
		
	}
	
	
	
	
}
