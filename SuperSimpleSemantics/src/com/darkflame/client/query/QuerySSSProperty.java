package com.darkflame.client.query;

import com.darkflame.client.semantic.SSSNode;
import com.darkflame.client.semantic.SSSProperty;


/** a more complex form of property that, as well as taking normal SSSNodes as
 * values or predicates, can also take querys **/
public class QuerySSSProperty extends SSSProperty {

	Query QueryForPred = null;
	public Query getQueryForPred() {
		return QueryForPred;
	}



	public Query getQueryForValue() {
		return QueryForValley;
	}
	Query QueryForValley = null;
	
	public QuerySSSProperty(SSSNode pred, SSSNode value) {
		
		super(pred, value);
	
	}

	
	
	public QuerySSSProperty(Query pred, SSSNode val) {		
		super(null,val);
		QueryForPred=pred;
	}
	public QuerySSSProperty(SSSNode pred, Query val) {		
		super(pred,null);
		QueryForValley = val;
	}
	public QuerySSSProperty(Query pred, Query val) {		
		super(null,null);
		QueryForPred=pred;
		QueryForValley = val;
	}
}
