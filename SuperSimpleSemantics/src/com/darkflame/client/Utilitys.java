package com.darkflame.client;

import java.util.logging.Logger;

import com.google.common.collect.ImmutableSet;

public class Utilitys {
	

	static Logger Log = Logger.getLogger("sss.Utilitys");
	
	/**
	 * accepted filename extensions that the isFilename will accept
	 */	
	public static final ImmutableSet<String> ACCEPTED_FILENAME_EXTENSIONS = ImmutableSet.of("sss", "txt","n3","ntlist","ntindex");

	
	/** replacement for a regex that  I could never get to work .
	 * This assumes filenames arnt too stupid (like this.is.a.file.txt or something)
	 * 
	 * Heres examples of what's considered a valid file;
	 * 
	 * 	isFilename("http://alicespccontents.txt");             //not file
		isFilename("http://www.blah.com/blah/blah/alicespccontents.txt");   //file
		isFilename("www.blah.com");  //not file
		isFilename("www.blah.com/alicespccontents.txt");  //file
		isFilename("http://lostagain.nl");  //not file
		isFilename("www.blah.com/alicespccontents.txt#test");  //not file
				
	 * 
	 * **/
	static public boolean isFilename(String filenameTotest) {
		
		boolean needsslash = false;
		boolean containedslash = false;

		Log.info("	testing:"+filenameTotest);
		
		if (filenameTotest.endsWith("\"") || filenameTotest.startsWith("\"")){

			Log.info("(not a filename as it ends or starts with a quote)");
			return false;
		}
		
		if (filenameTotest.contains("://")){
			filenameTotest=filenameTotest.substring(filenameTotest.lastIndexOf("://")+3);
			needsslash=true;
		}
		
		//crop to last last
		if (filenameTotest.contains("/")){						
			filenameTotest=filenameTotest.substring(filenameTotest.lastIndexOf("/")+1);
			containedslash=true;
		}
		if (filenameTotest.contains("/\\")){						
			filenameTotest=filenameTotest.substring(filenameTotest.lastIndexOf("/\\")+1);
			containedslash = true;
		}
		
		//now we ensure it had a slash if it needed one
		if (containedslash==false && needsslash==true){
			
			Log.info("(needed slash but didnt have one)"+filenameTotest);
			
			return false; //not a filename
		}
		
		//if theres one dot and no hash or then we assume its a file
		String filebits[] = filenameTotest.split("\\.");
		if (filebits.length==2){
			
			//we also exclude if the stuff after the . contains illegal characters for a filename (like " or @)
			//we also reject it if filebits if over 3
			//
			if (filebits[1].contains("\"") || filebits[1].contains("@") || !ACCEPTED_FILENAME_EXTENSIONS.contains(filebits[1])){
				
				
				return false;
			}
			
			
			if (!filenameTotest.contains("#")){
				
				return true;
											
			}
			Log.info("# present so its not a filename");
		}  else {
		//	Log.info("more then one dot:"+filenameTotest);
		}
		
		return false;
		
	}
}
