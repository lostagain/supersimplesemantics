package com.darkflame.client.semantic;

import java.util.ArrayList;

/** A property is just two nodes: a predicate (color) and a value (green)
 * With their powers combined, they are...a way to express a single property on a given subject**/
public class SSSProperty {

	public SSSNode getPred() {
		return pred;
	}



	public SSSNode getValue() {
		return value;
	}




	protected SSSNode pred;
	protected SSSNode value;
	
	

	static ArrayList<SSSProperty> allSSSPropertys= new ArrayList<SSSProperty>();
	
	public static void clearAllSSSPropertys() {
		SSSProperty.allSSSPropertys.clear();
	}



	public SSSProperty(SSSNode pred,SSSNode value) {
		
		super();
		this.pred = pred;
		this.value=value;
	}


	/** safely create a new SSSProperty, or return it if one with this exact pred and value exists already **/
	public static SSSProperty createSSSProperty(SSSNode pred,SSSNode value) {
		
		//check if it exists already and return it if it does
		for (SSSProperty prop : allSSSPropertys) {
			if ((prop.pred==pred)&&(prop.value==value)){
					return prop;			
			}
			
		}
		
		SSSProperty	np = new SSSProperty(pred,value);
		
		allSSSPropertys.add(np);
		
		//else we make one
		return np;
		
		
		
	}
	
	public String toString(){
		return pred.PURI+":"+value.PURI;
	}
	
	//needs hashcode and equals here
	@Override
	public boolean equals(Object testAgainst){
		
		if (testAgainst.getClass()==SSSProperty.class){			
			
			SSSProperty testingObject=(SSSProperty)testAgainst;
			//if the predicate and value match, then the objects are considered equal
			if ((testingObject.pred == this.pred)&&(testingObject.value==this.value)){
				return true;
			}
		}
		
		return false;
	}
	
	
	
}
