package com.darkflame.client.semantic;

public interface LoadingCallback {

	public void onFailure(Throwable caught);
	public void onSuccess(String result);
	
	
	
}
