package com.darkflame.client.semantic.demo;

import java.util.Iterator;
import java.util.logging.Logger;

import com.darkflame.client.semantic.SSSNode;

/** a demo collection of SSSNodes representing a knowledge base **/
public class DemoKnowledgeBase {

	static Logger Log = Logger.getLogger("sss.DemoKnowledgeBase");
	
	final static String baseURI = "examples/DefaultOntology.n3#";
	
	//properties all wood has
	static public SSSNode buoyantThings = SSSNode.createSSSNode("Buoyant",baseURI+"buoyantThings");
	static public SSSNode organicThings = SSSNode.createSSSNode("Organic",baseURI+"organicThings");
	
	//properties all drywood has
	static public SSSNode flamableThings = SSSNode.createSSSNode("Flamable",baseURI+"flamableThings");
	
	//types of wood
	static public SSSNode wood = SSSNode.createSSSNode("Wood",baseURI+"Wood",baseURI, new SSSNode[]{ buoyantThings,organicThings } );
	static public SSSNode drywood = SSSNode.createSSSNode("Dry Wood",baseURI+"DryWood", baseURI,new SSSNode[]{ wood,flamableThings } );
	
	//create nodes of types of wood
	static public  SSSNode oak = SSSNode.createSSSNode("Oak",baseURI+"Oak", baseURI,new SSSNode[]{ wood }  );
	static public SSSNode pine = SSSNode.createSSSNode("Pine",baseURI+"Pine",baseURI, new SSSNode[]{ wood } );
	static public SSSNode plywood = SSSNode.createSSSNode("Plywood",baseURI+"Plywood",baseURI, new SSSNode[]{ drywood } );
	

	//in future we should support static creation of common property sets
	//maybe;
	//SSSNodesWithCommonProperty greenthings = new SSSNodesWithCommonPropertySet.create(CommonPred,CommonVal, new SSSNode[]{ node1,node2,node3... } )
	
	
	static public void test(String testThis){
		
		SSSNode tt = SSSNode.getNodeByLabel(testThis);

		Log.info(" testing "+testThis+", listing all its classes:");
		
		Iterator<SSSNode> testc=tt.getAllClassesThisBelongsToo().iterator();
		
		while (testc.hasNext()) {
			
			SSSNode sssNode = (SSSNode) testc.next();
			
			Log.info(" -"+sssNode.getPURI());
			
			
			
		}
		
		
	}
	
}
