package com.darkflame.client.semantic;

//import com.google.gwt.user.client.rpc.IsSerializable;
//implements IsSerializable
public class SssStatement  {

	private static final long serialVersionUID = 1L; //no clue what this is for :-/
	
	 String subject = "";
	 String precident = "";
	 String value = "";
	 
	 public SssStatement(){
		 
	 }
	 public SssStatement(String subject, String precident, String value) {
		super();
		this.subject = subject;
		this.precident = precident;
		this.value = value;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getPrecident() {
		return precident;
	}
	public void setPrecident(String precident) {
		this.precident = precident;
	}
	public String getObject() {
		return value;
	}
	public void setObject(String object) {
		this.value = object;
	}
	public String asString()
	{
		return subject+" - "+precident+" - "+value;
		
	}
	
}
