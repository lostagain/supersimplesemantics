package com.darkflame.client.semantic;

/** This class stores the users query, as well as the ontologies it will use to get the result
 * **/

public class PreparedQuery {

	public String Query ="";
	public String[] OntURLS = {""};
	//Boolean Transative = false; // its mode
	
	
	public PreparedQuery(String Query,String[] OntURLS)
	{
		this.Query = Query;
		this.OntURLS= OntURLS;
		
	}
	
	
}
