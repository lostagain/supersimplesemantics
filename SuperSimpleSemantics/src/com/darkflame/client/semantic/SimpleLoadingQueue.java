package com.darkflame.client.semantic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Logger;

import com.darkflame.client.SuperSimpleSemantics;
import com.darkflame.client.interfaces.SSSGenericFileManager.FileCallbackError;
import com.darkflame.client.interfaces.SSSGenericFileManager.FileCallbackRunnable;
public class SimpleLoadingQueue {

	static Logger Log = Logger.getLogger("SSS.SimpleLoadingQueue");
	
	/**Loading limit. We shouldn't attempt to load too many files simultaneously
	 *Old or mobile browsers particularly don't like it.
	 * Most browsers these days support 6, but I stuck it on 5 by default to be safe **/
	final static int LOADING_LIMIT = 5;
	
	/** current number of files loading. Used so we don't attempt to spam
	 * too many simultaneous open requests at once **/
	static int CurrentOpenConnections = 0;
	
	static LinkedList<LoadRequest> pendingRequests = new LinkedList<LoadRequest>(); //First in, first out array
	
	
	
	/** add a new item to load 
	 * Loading starts straight away, unless LOADIND LIMIT is passed, 
	 * if so it gets put into a queue and waits for something else to load
	 * first.
	 * NOTE; You should always call "loadedAnItem()" at the top of the onResponse and onError callbacks passed 
	 * to this function**/
	public static void add(String fileURL, FileCallbackRunnable onResponse,
			FileCallbackError onError, boolean forcePost) {
		
		

		if (CurrentOpenConnections<LOADING_LIMIT)
		{
			CurrentOpenConnections++;
			
			Log.info("Loading: "+fileURL+", CurrentOpenConnections="+CurrentOpenConnections);
			
			//if we arnt loading too much right now we request it straight away.
			SuperSimpleSemantics.fileManager.getText(fileURL,
				onResponse,
				onError,
				forcePost);
			
			
		} else {
			//else we add it to the loading queue
			Log.info("Too much simultanious loading! "+CurrentOpenConnections+", adding to loading queue");
			
			pendingRequests.add(new LoadRequest(fileURL,onResponse,onError,forcePost));
		
		}
		
		
	}

	static public class LoadRequest
	{

		private String fileURL;
		private FileCallbackRunnable onResponse;
		private FileCallbackError onError;
		private boolean forcePost;
		

		public LoadRequest(String fileURL, FileCallbackRunnable onResponse,
				FileCallbackError onError, boolean forcePost) {
			
			this.fileURL = fileURL;
			this.onResponse = onResponse;
			this.onError = onError;
			this.forcePost = forcePost;
			
		}

		public void startloading(){
			
			CurrentOpenConnections++;
			
			SuperSimpleSemantics.fileManager.getText(fileURL,
				onResponse,
				onError,
				forcePost);
			
		}
	
		
	}

	public static void loadedAnItem() {
		
		CurrentOpenConnections--;
		
		if (CurrentOpenConnections<0){
			CurrentOpenConnections=0;
		}

		Log.info("Loaded item CurrentOpenConnections now="+CurrentOpenConnections);
		
		if (CurrentOpenConnections<LOADING_LIMIT && pendingRequests.size()>0)
		{
			
			
			LoadRequest nextonetoload = pendingRequests.remove();
			nextonetoload.startloading();
			
			
		}
		
		
	}

}
