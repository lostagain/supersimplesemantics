package com.darkflame.client.semantic;

import java.util.ArrayList;

public class SSSTriplet {

	public SSSNode getSubject() {
		return subject;
	}
	public SSSNode getPrecident() {
		return precident;
	}
	public SSSNode getValue() {
		return value;
	}
	
	
	SSSNode subject;
	SSSNode precident;	
	SSSNode value;
	
	/**
	 * returns the string;
	 *  subject.PURI+" "+precident.PURI+" "+value.PURI
	 *  for debug use mostly
	 */
	public String toString() {
		return subject.PURI+" "+precident.PURI+" "+value.PURI;
	}
	
	//Static list of all known triplets	
	//currently not using this to save ram
	static ArrayList<SSSTriplet> knownTriplets= new ArrayList<SSSTriplet>();
	
	public static ArrayList<SSSTriplet> getKnownTriplets() {
		return knownTriplets;
	}
	public static void clearKnownTriplets() {
		SSSTriplet.knownTriplets.clear();
	}
	/** 
	 * @param subject - eg Apple
	 * @param precident - eg Colour
	 * @param value - eg Green
	 */
	public SSSTriplet(SSSNode subject, SSSNode precident, SSSNode value) {
		super();
		this.subject = subject;
		this.precident = precident;
		this.value = value;
	}

	/** safely create a new triplet, or return it if it exists already **/
	
	/*
	public static SSSNode createSSSTriplet(SSSNode subject, SSSNode precident, SSSNode value) {

		

		SSSTriplet newnode = SSSNode.getPredicate(pURI);
		
		if (newnode==null||(newnode==SSSNode.NOTFOUND)){

			newnode = new SSSTriplet(subject, precident, value);

		} else {

		}

		return newnode;

	}*/


	@Override
	public boolean equals(Object c){

		if (c.getClass()==getClass()){

			SSSTriplet testme = (SSSTriplet) c;
			if (       (testme.precident == this.precident)
					&& (testme.value == this.value)
					&& (testme.subject == this.subject)
					) 
			{

				return true;
			}


			return false;

		}

		return false;
	}




}
