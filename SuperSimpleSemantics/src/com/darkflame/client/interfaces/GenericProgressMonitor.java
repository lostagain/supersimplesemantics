package com.darkflame.client.interfaces;

/** allows the user to see the (potentially long) loading progress of SSSIndexs **/
public interface GenericProgressMonitor {
	
	/** set the total loading units, if a loading monitor has been supplied **/
	public void setTotalProgressUnits(int i);
	
	/** adds to the total loading units, if a loading monitor has been supplied **/
	public void addToTotalProgressUnits(int i);
	
	/** sets what the loading is currently doing **/
	public void setCurrentProcess(String message);
	
	/** steps the loading monitor forward **/
	public void stepProgressForward();
	
	/** set the current progress in the task units **/
	public void setCurrentProgress(int i);

	
}
