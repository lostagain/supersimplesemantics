package com.darkflame.client.interfaces;

/** As the semantic processing is a long, messy, complex thing<br>
 * you might want to supply your own debugger rather then using the default java<br>
 * system, you can do that by supply a class with this interface <br>
 * Note;<br>
 * This class is here partly due to the split from TheGreenFruitEngine project,<br>
 * **/
public interface GenericDebugDisplayer {

	public void log(String logthis);
	public void error(String logthis);
	public void info(String logthis);
	public void log(String logthis, String color);
	
}
