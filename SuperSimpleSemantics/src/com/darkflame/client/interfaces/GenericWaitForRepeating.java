package com.darkflame.client.interfaces;

/** Much like "wait for" this is to allow interface or other updates needed 
 * while the semantic engine is processing.
 * In this case, its designed for tasks that repeat within the engine.
 * If your using GWT, you can make a scheduleIncremental that implements this, using "MyRepeatingCommand instead of "RepeatingCommand**/
public interface GenericWaitForRepeating {

	public void scheduleAfter(MyRepeatingCommand runAfter);
	
	
	
	
	 /**
     * Returns true if the RepeatingCommand should be invoked again.
     */
	public interface MyRepeatingCommand {
	    /**
	     * Returns true if the RepeatingCommand should be invoked again.
	     */
	    boolean execute();
	  }
	
}
