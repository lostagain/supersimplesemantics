package com.darkflame.client.interfaces;

/** interface for a generic "wait" function **/
public interface GenericWaitFor {

	public void scheduleAfter(Runnable runAfter);
	
	
	
}
