package com.darkflame.client.interfaces;

import com.darkflame.client.semantic.SSSIndex;

/** You can supply a class that interfaces this in order 
 * to monitor the contents of the indexes being loaded.**/
public interface GenericIndexDisplayer {
	
	public void addIndexToDisplayer(SSSIndex index);
	
}
