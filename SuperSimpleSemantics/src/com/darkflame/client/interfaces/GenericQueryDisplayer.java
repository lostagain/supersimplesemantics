package com.darkflame.client.interfaces;

import com.darkflame.client.query.Query;


/** a way to visualise a query 
 * Normally this would be implemented in such a way as to
 * contain copies of itself for sub-queries. 
 * **/
public interface GenericQueryDisplayer {	

	/** sets up the widget and then runs populate automatically 
	 * nb; might not be needed as a separate function.**/
	public void setQuery(Query queryToDisplay);
	
	/** sets the raw result data. ie, this could be the matching nodes labels **/
	public void setResultData(String data);
	
	/** just updates/fills the contents of the displayer with the specified query**/
	public void populateWithQuery(Query fillWithThisQuery);
	
	
	
	
	
}
