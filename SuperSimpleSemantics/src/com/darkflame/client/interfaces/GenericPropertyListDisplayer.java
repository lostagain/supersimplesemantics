package com.darkflame.client.interfaces;

import com.darkflame.client.semantic.SSSNode;
import com.darkflame.client.semantic.SSSNodesWithCommonProperty;
import com.google.common.collect.HashMultimap;

public interface GenericPropertyListDisplayer {

	
	public void  addCPLToDisplayer(SSSNodesWithCommonProperty thisset);
	
	public void  removeCPLToDisplayer(SSSNodesWithCommonProperty thisset);
	
	public void setCPLDatabase(HashMultimap<SSSNode, SSSNodesWithCommonProperty> globalNodesWithPropertyListByPredicate);
	
}
