package com.darkflame.client;

import com.darkflame.client.interfaces.GenericWaitForRepeating;


/** Much like "wait for" this is to allow interface or other updates needed 
 * while the semantic engine is processing.
 * In this case, its designed for tasks that repeat within the engine.
 * If your using GWT, you can think of this as a scheduleIncremental **/
public class DefaultWaitForRepeating implements GenericWaitForRepeating {

	
	public void scheduleAfter(final MyRepeatingCommand runAfter) {	
		
		Boolean runAgain = runAfter.execute();
		
		if (runAgain){
			scheduleAfter(runAfter);
		}
		
	}
	
	
	

}
