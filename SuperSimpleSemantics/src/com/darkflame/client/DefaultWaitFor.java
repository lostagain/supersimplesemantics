package com.darkflame.client;

import com.darkflame.client.interfaces.GenericWaitFor;


/** The default near-empty class used for what happens in-between semantic process's **/
public class DefaultWaitFor implements GenericWaitFor {

	@Override
	public void scheduleAfter(final Runnable runAfter) {		
		
		//by default we wait for nothing, so we just run straight away
		runAfter.run();
		
	}

}
